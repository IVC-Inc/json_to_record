# json_to_record extension

This PostgreSQL extension provides functions that work like 
`json_populate_record` and `json_populate_recordset` but don't require the 
caller to provide a record of the result type as an argument. Instead the caller
provides an anonymous type descriptor as part of the call, using the `SELECT`
stetement's standard syntax.

The functions are:

* `json_to_record (from_json json, use_json_as_text boolean DEFAULT false)`
* `json_to_recordset (from_json json, use_json_as_text boolean DEFAULT false)`

`json_to_record` returns the pseudotype `RECORD` and `json_to_recordset` returns a `SETOF RECORD`.

Because these functions require a type expression, they can only be used in the
`FROM` clause of a `SELECT` statement, which is the only place that such an
expression is legal.


Examples:

	select * from json_to_record('{"a":1,"b":"foo","c":"bar"}',true) 
                              as x(a int, b text, d text);
	 a |  b  | d 
	---+-----+---
	 1 | foo | 
	(1 row)

	select * from json_to_recordset('[{"a":1,"b":"foo","d":false},
                                      {"a":2,"b":"bar","c":true}]',false) 
                                  as x(a int, b text, c boolean);
	 a |  b  | c 
	---+-----+---
	 1 | foo | 
	 2 | bar | t
	(2 rows)

This development was sponsored by [IVC](http://ivc.com) .
