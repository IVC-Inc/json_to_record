
-- complain if script is sourced in psql, rather than via CREATE EXTENSION
\echo Use "CREATE EXTENSION json_to_record" to load this file. \quit

CREATE FUNCTION json_to_record(from_json json, use_json_as_text boolean default false)
RETURNS record
AS 'MODULE_PATHNAME'
LANGUAGE C IMMUTABLE;

CREATE FUNCTION json_to_recordset(from_json json, use_json_as_text boolean default false)
RETURNS SETOF record
AS 'MODULE_PATHNAME'
LANGUAGE C IMMUTABLE;
