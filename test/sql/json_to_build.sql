select * from json_to_record('{"a":1,"b":"foo","c":"bar"}',true) 
    as x(a int, b text, d text);

select * from json_to_recordset('[{"a":1,"b":"foo","d":false},{"a":2,"b":"bar","c":true}]',false) 
    as x(a int, b text, c boolean);


